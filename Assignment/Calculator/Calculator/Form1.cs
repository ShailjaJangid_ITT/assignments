﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        string Operator=string.Empty;
        bool DivideByZero = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            display.Text = display.Text+"0";
        }

        private void buttonDot_Click(object sender, EventArgs e)
        {
            int counter = 0;
            if (display.Text != "")
            {
                //Calculations();
                if (display.Text[display.Text.Length - 1] != '.')
                {
                    char[] signs = { '/', '*', '-', '+'};
                    foreach (char character in signs)
                    {
                        if (display.Text.Contains(character))
                        {
                            //int index = display.Text.LastIndexOf(c);
                            string[] SignSplitted = display.Text.Split(character);
                            //if (!display.Text.Substring(index, display.Text.Length - 1).Contains('.'))
                            //{
                            counter++;
                            if (!SignSplitted[1].Contains('.'))
                                display.Text = display.Text + "0.";
                            //}
                        }
                    }
                    if (counter == 0)
                    {
                        if (!display.Text.Contains('.'))
                           display.Text = display.Text + ".";
                    }
                }
            }
            else
            {
                display.Text = display.Text + "0.";
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string expression = string.Empty;
            if (display.Text != string.Empty)
            {
                //if (display.Text[display.Text.Length - 1] != Convert.ToChar("+"))
                //{
                    Calculations();
                    if (display.Text[display.Text.Length - 1] == Convert.ToChar("*") || display.Text[display.Text.Length - 1] == Convert.ToChar("-") || display.Text[display.Text.Length - 1] == Convert.ToChar("/") || display.Text[display.Text.Length - 1] == Convert.ToChar("%"))
                    {
                        expression = RemoveLastCharacter(display.Text);
                        display.Text = expression + "+";
                    }
                    else
                    {
                        if (!DivideByZero )
                            display.Text = display.Text + "+";
                    }
                //}
            }
            Operator= "+";
        }

        private void buttonEqual_Click(object sender, EventArgs e)
        {
            Calculations();
        }

        private void buttonSub_Click(object sender, EventArgs e)
        {
            string expression = string.Empty;
            if (display.Text != string.Empty)
            {
                //  num1 = float.Parse(display.Text);
                //if (display.Text[display.Text.Length - 1] != Convert.ToChar("-"))
                //{
                    Calculations();
                    if (display.Text[display.Text.Length - 1] == Convert.ToChar("*") || display.Text[display.Text.Length - 1] == Convert.ToChar("+") ||display.Text[display.Text.Length - 1] == Convert.ToChar("/") )
                    {
                       expression = RemoveLastCharacter(display.Text);
                        display.Text = expression + "-";
                    }
                    else
                    {
                        if (!DivideByZero )
                            display.Text = display.Text + "-";
                        //txtExpression.Text = txtExpression.Text + txtResult.Text;
                    }
                //}
                Operator = "-";
            }
        }

        private void buttonMul_Click(object sender, EventArgs e)
        {
            string expression = string.Empty;
            if (display.Text != string.Empty)
            {
                Calculations();
                //if (display.Text[display.Text.Length - 1] != Convert.ToChar("*"))
                //{
                    if (display.Text[display.Text.Length - 1] == Convert.ToChar("-") ||display.Text[display.Text.Length - 1] == Convert.ToChar("+") || display.Text[display.Text.Length - 1] == Convert.ToChar("/") )
                    {
                        expression = RemoveLastCharacter(display.Text);
                       display.Text = expression + "*";
                    }
                    else
                    {
                        if (!DivideByZero )
                           display.Text = display.Text + "*";
                        //txtExpression.Text = txtExpression.Text + txtResult.Text;
                    }
                }
            //}
            Operator = "*";
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            string expression = string.Empty;
            if (display.Text != string.Empty)
            {
                Calculations();
                //if (display.Text[display.Text.Length - 1] != Convert.ToChar("/"))
                //{
                    if (display.Text[display.Text.Length - 1] == Convert.ToChar("*") || display.Text[display.Text.Length - 1] == Convert.ToChar("+") || display.Text[display.Text.Length - 1] == Convert.ToChar("/") )
                    {
                        expression = RemoveLastCharacter(display.Text);
                        display.Text = expression + "/";
                    }
                    else
                    {
                        if (!DivideByZero )
                            display.Text = display.Text + "/";
                        //txtExpression.Text = txtExpression.Text + txtResult.Text;
                    }
                }
            //}
            Operator = "/";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            display.Text = display.Text+"8";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "4";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "7";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "9";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "6";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            display.Text = display.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            display.Text = display.Text +"3";
        }

        private void display_TextChanged(object sender, EventArgs e)
        {

        }

        public long CalculateIntegerResult(string operation, long number1, long number2)
        {
            long result = 0;
            try
            {
                if (display.Text.Contains("+") || display.Text.Contains("-") ||display.Text.Contains("*") ||display.Text.Contains("/") )
                {
                    switch (operation)
                    {
                        case "+":
                            result = number1 + number2;
                            break;
                        case "-":
                            result = number1 - number2;
                            break;
                        case "*":
                            result = number1 * number2;
                            break;
                        case "/":
                            result = number1 / number2;
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
            catch
            {
                return result;
            }
        }

        public double CalculateFloatResult(string operation, double number1, double number2)
        {
           double result = 0;
            try
            {
                if (display.Text.Contains("+") ||display.Text.Contains("-") || display.Text.Contains("*") || display.Text.Contains("/"))
                {
                    switch (operation)
                    {
                        case "+":
                            result = number1 + number2;
                            break;
                        case "-":
                            result = number1 - number2;
                            break;
                        case "*":
                            result = number1 * number2;
                            break;
                        case "/":
                            result = number1 / number2;
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
            catch
            {
                return result;
            }
        }

        public void Calculations()
        {
            try
            {
                if (display.Text.Contains('+'))
                {
                    string[] splitted = display.Text.Split('+');
                    if (splitted[1] != "")
                    {
                        if (splitted[0].Contains('.') || splitted[1].Contains('.'))
                        {
                            double floatResult = CalculateFloatResult("+",double.Parse(splitted[0]),double.Parse(splitted[1]));
                            display.Text = Convert.ToString(floatResult);
                        }
                        else
                        {
                            long integerResult = CalculateIntegerResult("+", long.Parse(splitted[0]), long.Parse(splitted[1]));
                           display.Text = Convert.ToString(integerResult);
                        }
                    }
                }
                if (display.Text.Contains('-'))
                {
                    string[] splitted = display.Text.Split('-');
                    if (splitted[1] != "")
                    {
                        if (splitted[0].Contains('.') || splitted[1].Contains('.'))
                        {
                            double floatResult = CalculateFloatResult("-", double.Parse(splitted[0]), double.Parse(splitted[1]));
                            display.Text = Convert.ToString(floatResult);
                        }
                        else
                        {
                            long integerResult = CalculateIntegerResult("-", long.Parse(splitted[0]), long.Parse(splitted[1]));
                            display.Text = Convert.ToString(integerResult);
                        }
                    }
                }
                if (display.Text.Contains('*'))
                {
                    string[] splitted = display.Text.Split('*');
                    if (splitted[1] != "")
                    {
                        if (splitted[0].Contains('.') || splitted[1].Contains('.'))
                        {
                            double floatResult = CalculateFloatResult("*", double.Parse(splitted[0]), double.Parse(splitted[1]));
                            display.Text = Convert.ToString(floatResult);
                        }
                        else
                        {
                            long integerResult = CalculateIntegerResult("*", long.Parse(splitted[0]), long.Parse(splitted[1]));
                            display.Text = Convert.ToString(integerResult);
                        }
                    }
                }
                if (display.Text.Contains('/'))
                {
                    string[] splitted = display.Text.Split('/');
                    if (splitted[1] != "")
                    {
                        if (double.Parse(splitted[1]) != 0)
                        {
                            double result = double.Parse(splitted[0]) / double.Parse(splitted[1]);
                            display.Text = result.ToString();
                        }
                        else {
                            MessageBox.Show("Cannot divide by Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            DivideByZero = true;
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            display.Text = string.Empty;
        }

        private void Back_Click(object sender, EventArgs e)
        {
            string back = RemoveLastCharacter(display.Text);
            display.Text = back;
        }
        
        public string RemoveLastCharacter(string fulltext)
        {
            string  lastRemoved= string.Empty;
            char[] text =fulltext.ToCharArray();
            for (int i = 0; i < text.Length - 1; i++)
            {
                lastRemoved += text[i];
            }
            return lastRemoved;
        }

        

    }
}
