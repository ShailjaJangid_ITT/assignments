﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaAutomobilesDatabaseFirst
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                if (VehicleType.Text != null && VehicleName.Text != null && VehicleDescription.Text != null)
                {
                    TeslaEntities teslaEntities = new TeslaEntities();
                    VehicleDetail vehicleDetails = new VehicleDetail();
                    vehicleDetails.VehicleId = Convert.ToInt32(VehicleType.Text);
                    vehicleDetails.VehicleName = VehicleName.Text;
                    vehicleDetails.VehicleDescription = VehicleDescription.Text;
                    teslaEntities.VehicleDetails.Add(vehicleDetails);
                    int response = teslaEntities.SaveChanges();
                    if (response > 0)
                        MessageBox.Show("Added Successfully");
                    else
                        MessageBox.Show("Failed");
                }
            }
            catch
            {
                MessageBox.Show("Either the database connectivity is failed or Input is not in proper format");
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                string vehicleName = VehicleName.Text;

                TeslaEntities teslaEntities = new TeslaEntities();

                var vehicle = (from d in teslaEntities.VehicleDetails where d.VehicleName == vehicleName select d).Single();

                teslaEntities.VehicleDetails.Remove(vehicle);
                int response = teslaEntities.SaveChanges();
                if (response > 0)
                    MessageBox.Show("Deleted Successfully");
                else
                    MessageBox.Show("Failed");
            }
            catch
            {
                MessageBox.Show("Failed");

            }
        }

        private void Update_Click(object sender, EventArgs e)
        {
            try
            {
                VehicleType.Text = "";
                VehicleDescription.Text = "";
                string vehicleName = VehicleName.Text;
                TeslaEntities teslaEntities = new TeslaEntities();
                var vehicle = (from d in teslaEntities.VehicleDetails where d.VehicleName == vehicleName select d).Single();
                if (vehicle != null)
                {
                    teslaEntities.VehicleDetails.Remove(vehicle);
                    teslaEntities.SaveChanges();
                    MessageBox.Show("Available");
                    VehicleName.Text = "";
                    information.Text = "Please provide new details and click Add";
                    VehicleType.Text = "";
                    VehicleDescription.Text = "";
                    VehicleType = null;
                    //VehicleDetail vehicleDetails = new VehicleDetail();
                    //vehicleDetails.VehicleId = Convert.ToInt32(VehicleType.Text);
                    //vehicleDetails.VehicleName = VehicleName.Text;
                    //vehicleDetails.VehicleDescription = VehicleDescription.Text;
                    Add_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Not available");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not available");
            }
        }

        private void Find_Click(object sender, EventArgs e)
        {
            try
            {
                string vehicleName = VehicleName.Text;
                TeslaEntities teslaEntities = new TeslaEntities();
                var vehicle = (from d in teslaEntities.VehicleDetails where d.VehicleName == vehicleName select d).Single();
                if (vehicle != null)
                {
                    MessageBox.Show("Available");
                }
                else
                    MessageBox.Show("Not available");
            }
            catch 
            {
                MessageBox.Show("Not available");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("hello");
            //// TODO: This line of code loads data into the 'teslaDataSet.VehicleDetails' table. You can move, or remove it, as needed.
            this.vehicleDetailsTableAdapter.Fill(this.teslaDataSet.VehicleDetails);
            TeslaEntities teslaEntities = new TeslaEntities();
            dataView.DataSource = teslaEntities.VehicleDetails.ToList();
            teslaEntities.SaveChanges();

        }

        private void Display_Click(object sender, EventArgs e)
        {
            TeslaEntities teslaEntities = new TeslaEntities();
            dataView.DataSource = teslaEntities.VehicleDetails.ToList();
            teslaEntities.SaveChanges();
        }





    }
}
