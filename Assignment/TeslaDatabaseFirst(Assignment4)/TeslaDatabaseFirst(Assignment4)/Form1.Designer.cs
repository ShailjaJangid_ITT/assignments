﻿namespace TeslaAutomobilesDatabaseFirst
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Update = new System.Windows.Forms.Button();
            this.Find = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.VehicleDescription = new System.Windows.Forms.TextBox();
            this.VehicleName = new System.Windows.Forms.TextBox();
            this.VehicleType = new System.Windows.Forms.TextBox();
            this.Buses = new System.Windows.Forms.Label();
            this.Trucks = new System.Windows.Forms.Label();
            this.Cars = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.information = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dataView = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teslaDataSet = new TeslaAutomobilesDatabaseFirst.TeslaDataSet();
            this.Display = new System.Windows.Forms.Button();
            this.vehicleDetailsTableAdapter = new TeslaAutomobilesDatabaseFirst.TeslaDataSetTableAdapters.VehicleDetailsTableAdapter();
            this.label4 = new System.Windows.Forms.Label();
            this.AddOption = new System.Windows.Forms.RadioButton();
            this.DeleteOption = new System.Windows.Forms.RadioButton();
            this.UpdateOption = new System.Windows.Forms.RadioButton();
            this.SearchOption = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome To Tesla";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vehicle Type";
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(17, 371);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(61, 23);
            this.Add.TabIndex = 6;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(82, 371);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(56, 23);
            this.Delete.TabIndex = 7;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Update
            // 
            this.Update.Location = new System.Drawing.Point(144, 371);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(51, 23);
            this.Update.TabIndex = 8;
            this.Update.Text = "Update";
            this.Update.UseVisualStyleBackColor = true;
            this.Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // Find
            // 
            this.Find.Location = new System.Drawing.Point(210, 371);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(62, 23);
            this.Find.TabIndex = 9;
            this.Find.Text = "Find";
            this.Find.UseVisualStyleBackColor = true;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 287);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Vehicle Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 319);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Vehicle Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "VehicleDescription";
            // 
            // VehicleDescription
            // 
            this.VehicleDescription.Location = new System.Drawing.Point(142, 337);
            this.VehicleDescription.Name = "VehicleDescription";
            this.VehicleDescription.Size = new System.Drawing.Size(130, 20);
            this.VehicleDescription.TabIndex = 13;
            // 
            // VehicleName
            // 
            this.VehicleName.Location = new System.Drawing.Point(144, 311);
            this.VehicleName.Name = "VehicleName";
            this.VehicleName.Size = new System.Drawing.Size(130, 20);
            this.VehicleName.TabIndex = 14;
            // 
            // VehicleType
            // 
            this.VehicleType.Location = new System.Drawing.Point(142, 280);
            this.VehicleType.Name = "VehicleType";
            this.VehicleType.Size = new System.Drawing.Size(130, 20);
            this.VehicleType.TabIndex = 15;
            // 
            // Buses
            // 
            this.Buses.AutoSize = true;
            this.Buses.Location = new System.Drawing.Point(27, 131);
            this.Buses.Name = "Buses";
            this.Buses.Size = new System.Drawing.Size(51, 13);
            this.Buses.TabIndex = 4;
            this.Buses.Text = "Buses : 3";
            // 
            // Trucks
            // 
            this.Trucks.AutoSize = true;
            this.Trucks.Location = new System.Drawing.Point(23, 109);
            this.Trucks.Name = "Trucks";
            this.Trucks.Size = new System.Drawing.Size(55, 13);
            this.Trucks.TabIndex = 3;
            this.Trucks.Text = "Trucks : 2";
            // 
            // Cars
            // 
            this.Cars.AutoSize = true;
            this.Cars.Location = new System.Drawing.Point(27, 86);
            this.Cars.Name = "Cars";
            this.Cars.Size = new System.Drawing.Size(43, 13);
            this.Cars.TabIndex = 2;
            this.Cars.Text = "Cars : 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 408);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 26);
            this.label3.TabIndex = 16;
            this.label3.Text = "For Delete, Update and Find operations, provide\r\nonly Vehicle Name";
            // 
            // information
            // 
            this.information.AutoSize = true;
            this.information.Location = new System.Drawing.Point(28, 167);
            this.information.Name = "information";
            this.information.Size = new System.Drawing.Size(0, 13);
            this.information.TabIndex = 17;
            // 
            // dataView
            // 
            this.dataView.AutoGenerateColumns = false;
            this.dataView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.vehicleNameDataGridViewTextBoxColumn,
            this.vehicleDescriptionDataGridViewTextBoxColumn,
            this.vehicleIdDataGridViewTextBoxColumn});
            this.dataView.DataSource = this.vehicleDetailsBindingSource;
            this.dataView.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.Location = new System.Drawing.Point(293, 30);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(456, 350);
            this.dataView.TabIndex = 18;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehicleNameDataGridViewTextBoxColumn
            // 
            this.vehicleNameDataGridViewTextBoxColumn.DataPropertyName = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.HeaderText = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.Name = "vehicleNameDataGridViewTextBoxColumn";
            // 
            // vehicleDescriptionDataGridViewTextBoxColumn
            // 
            this.vehicleDescriptionDataGridViewTextBoxColumn.DataPropertyName = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.HeaderText = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.Name = "vehicleDescriptionDataGridViewTextBoxColumn";
            // 
            // vehicleIdDataGridViewTextBoxColumn
            // 
            this.vehicleIdDataGridViewTextBoxColumn.DataPropertyName = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.HeaderText = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.Name = "vehicleIdDataGridViewTextBoxColumn";
            // 
            // vehicleDetailsBindingSource
            // 
            this.vehicleDetailsBindingSource.DataMember = "VehicleDetails";
            this.vehicleDetailsBindingSource.DataSource = this.teslaDataSet;
            // 
            // teslaDataSet
            // 
            this.teslaDataSet.DataSetName = "TeslaDataSet";
            this.teslaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Display
            // 
            this.Display.Location = new System.Drawing.Point(82, 437);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(113, 23);
            this.Display.TabIndex = 19;
            this.Display.Text = "Display Vehicles";
            this.Display.UseVisualStyleBackColor = true;
            this.Display.Click += new System.EventHandler(this.Display_Click);
            // 
            // vehicleDetailsTableAdapter
            // 
            this.vehicleDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Operations Available";
            // 
            // AddOption
            // 
            this.AddOption.AutoSize = true;
            this.AddOption.Location = new System.Drawing.Point(26, 192);
            this.AddOption.Name = "AddOption";
            this.AddOption.Size = new System.Drawing.Size(44, 17);
            this.AddOption.TabIndex = 21;
            this.AddOption.TabStop = true;
            this.AddOption.Text = "Add";
            this.AddOption.UseVisualStyleBackColor = true;
            // 
            // DeleteOption
            // 
            this.DeleteOption.AutoSize = true;
            this.DeleteOption.Location = new System.Drawing.Point(157, 192);
            this.DeleteOption.Name = "DeleteOption";
            this.DeleteOption.Size = new System.Drawing.Size(56, 17);
            this.DeleteOption.TabIndex = 22;
            this.DeleteOption.TabStop = true;
            this.DeleteOption.Text = "Delete";
            this.DeleteOption.UseVisualStyleBackColor = true;
            // 
            // UpdateOption
            // 
            this.UpdateOption.AutoSize = true;
            this.UpdateOption.Location = new System.Drawing.Point(26, 215);
            this.UpdateOption.Name = "UpdateOption";
            this.UpdateOption.Size = new System.Drawing.Size(60, 17);
            this.UpdateOption.TabIndex = 23;
            this.UpdateOption.TabStop = true;
            this.UpdateOption.Text = "Update";
            this.UpdateOption.UseVisualStyleBackColor = true;
            // 
            // SearchOption
            // 
            this.SearchOption.AutoSize = true;
            this.SearchOption.Location = new System.Drawing.Point(157, 215);
            this.SearchOption.Name = "SearchOption";
            this.SearchOption.Size = new System.Drawing.Size(59, 17);
            this.SearchOption.TabIndex = 24;
            this.SearchOption.TabStop = true;
            this.SearchOption.Text = "Search";
            this.SearchOption.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 472);
            this.Controls.Add(this.SearchOption);
            this.Controls.Add(this.UpdateOption);
            this.Controls.Add(this.DeleteOption);
            this.Controls.Add(this.AddOption);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Display);
            this.Controls.Add(this.dataView);
            this.Controls.Add(this.information);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.VehicleType);
            this.Controls.Add(this.VehicleName);
            this.Controls.Add(this.VehicleDescription);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.Update);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.Buses);
            this.Controls.Add(this.Trucks);
            this.Controls.Add(this.Cars);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Update;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox VehicleDescription;
        private System.Windows.Forms.TextBox VehicleName;
        private System.Windows.Forms.TextBox VehicleType;
        private System.Windows.Forms.Label Buses;
        private System.Windows.Forms.Label Trucks;
        private System.Windows.Forms.Label Cars;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label information;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView dataView;
        private System.Windows.Forms.Button Display;
        private TeslaDataSet teslaDataSet;
        private System.Windows.Forms.BindingSource vehicleDetailsBindingSource;
        private TeslaDataSetTableAdapters.VehicleDetailsTableAdapter vehicleDetailsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton AddOption;
        private System.Windows.Forms.RadioButton DeleteOption;
        private System.Windows.Forms.RadioButton UpdateOption;
        private System.Windows.Forms.RadioButton SearchOption;
    }
}

