﻿namespace TeslaDatabaseFirst_Assignment4_
{
    partial class Tesla_Automobiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SearchOption = new System.Windows.Forms.RadioButton();
            this.DeleteOption = new System.Windows.Forms.RadioButton();
            this.UpdateOption = new System.Windows.Forms.RadioButton();
            this.AddOption = new System.Windows.Forms.RadioButton();
            this.dataView = new System.Windows.Forms.DataGridView();
            this.teslaDataSet = new TeslaDatabaseFirst_Assignment4_.TeslaDataSet();
            this.vehicleDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleDetailTableAdapter = new TeslaDatabaseFirst_Assignment4_.TeslaDataSetTableAdapters.VehicleDetailTableAdapter();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleType = new System.Windows.Forms.Label();
            this.vehicleDescription = new System.Windows.Forms.Label();
            this.vehicleName = new System.Windows.Forms.Label();
            this.vehicleDescriptionText = new System.Windows.Forms.TextBox();
            this.vehicleNameText = new System.Windows.Forms.TextBox();
            this.vehicleTypeText = new System.Windows.Forms.TextBox();
            this.operationButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(312, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Tesla Automobiles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Operations Available";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Buses : 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Trucks : 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Car : 1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Vehicle Type";
            // 
            // SearchOption
            // 
            this.SearchOption.AutoSize = true;
            this.SearchOption.Location = new System.Drawing.Point(154, 227);
            this.SearchOption.Name = "SearchOption";
            this.SearchOption.Size = new System.Drawing.Size(59, 17);
            this.SearchOption.TabIndex = 6;
            this.SearchOption.TabStop = true;
            this.SearchOption.Text = "Search";
            this.SearchOption.UseVisualStyleBackColor = true;
            this.SearchOption.CheckedChanged += new System.EventHandler(this.SearchOption_CheckedChanged);
            // 
            // DeleteOption
            // 
            this.DeleteOption.AutoSize = true;
            this.DeleteOption.Location = new System.Drawing.Point(154, 204);
            this.DeleteOption.Name = "DeleteOption";
            this.DeleteOption.Size = new System.Drawing.Size(56, 17);
            this.DeleteOption.TabIndex = 7;
            this.DeleteOption.TabStop = true;
            this.DeleteOption.Text = "Delete";
            this.DeleteOption.UseVisualStyleBackColor = true;
            this.DeleteOption.CheckedChanged += new System.EventHandler(this.DeleteOption_CheckedChanged);
            // 
            // UpdateOption
            // 
            this.UpdateOption.AutoSize = true;
            this.UpdateOption.Location = new System.Drawing.Point(32, 227);
            this.UpdateOption.Name = "UpdateOption";
            this.UpdateOption.Size = new System.Drawing.Size(60, 17);
            this.UpdateOption.TabIndex = 8;
            this.UpdateOption.TabStop = true;
            this.UpdateOption.Text = "Update";
            this.UpdateOption.UseVisualStyleBackColor = true;
            this.UpdateOption.CheckedChanged += new System.EventHandler(this.UpdateOption_CheckedChanged);
            // 
            // AddOption
            // 
            this.AddOption.AutoSize = true;
            this.AddOption.Location = new System.Drawing.Point(32, 204);
            this.AddOption.Name = "AddOption";
            this.AddOption.Size = new System.Drawing.Size(44, 17);
            this.AddOption.TabIndex = 9;
            this.AddOption.TabStop = true;
            this.AddOption.Text = "Add";
            this.AddOption.UseVisualStyleBackColor = true;
            this.AddOption.CheckedChanged += new System.EventHandler(this.AddOption_CheckedChanged);
            // 
            // dataView
            // 
            this.dataView.AutoGenerateColumns = false;
            this.dataView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.vehicleNameDataGridViewTextBoxColumn,
            this.vehicleDescriptionDataGridViewTextBoxColumn,
            this.vehicleIdDataGridViewTextBoxColumn});
            this.dataView.DataSource = this.vehicleDetailBindingSource;
            this.dataView.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.Location = new System.Drawing.Point(343, 56);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(432, 392);
            this.dataView.TabIndex = 10;
            // 
            // teslaDataSet
            // 
            this.teslaDataSet.DataSetName = "TeslaDataSet";
            this.teslaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vehicleDetailBindingSource
            // 
            this.vehicleDetailBindingSource.DataMember = "VehicleDetail";
            this.vehicleDetailBindingSource.DataSource = this.teslaDataSet;
            // 
            // vehicleDetailTableAdapter
            // 
            this.vehicleDetailTableAdapter.ClearBeforeFill = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehicleNameDataGridViewTextBoxColumn
            // 
            this.vehicleNameDataGridViewTextBoxColumn.DataPropertyName = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.HeaderText = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.Name = "vehicleNameDataGridViewTextBoxColumn";
            // 
            // vehicleDescriptionDataGridViewTextBoxColumn
            // 
            this.vehicleDescriptionDataGridViewTextBoxColumn.DataPropertyName = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.HeaderText = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.Name = "vehicleDescriptionDataGridViewTextBoxColumn";
            // 
            // vehicleIdDataGridViewTextBoxColumn
            // 
            this.vehicleIdDataGridViewTextBoxColumn.DataPropertyName = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.HeaderText = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.Name = "vehicleIdDataGridViewTextBoxColumn";
            // 
            // vehicleType
            // 
            this.vehicleType.AutoSize = true;
            this.vehicleType.Location = new System.Drawing.Point(29, 291);
            this.vehicleType.Name = "vehicleType";
            this.vehicleType.Size = new System.Drawing.Size(69, 13);
            this.vehicleType.TabIndex = 11;
            this.vehicleType.Text = "Vehicle Type";
            // 
            // vehicleDescription
            // 
            this.vehicleDescription.AutoSize = true;
            this.vehicleDescription.Location = new System.Drawing.Point(29, 349);
            this.vehicleDescription.Name = "vehicleDescription";
            this.vehicleDescription.Size = new System.Drawing.Size(98, 13);
            this.vehicleDescription.TabIndex = 12;
            this.vehicleDescription.Text = "Vehicle Description";
            // 
            // vehicleName
            // 
            this.vehicleName.AutoSize = true;
            this.vehicleName.Location = new System.Drawing.Point(29, 320);
            this.vehicleName.Name = "vehicleName";
            this.vehicleName.Size = new System.Drawing.Size(73, 13);
            this.vehicleName.TabIndex = 13;
            this.vehicleName.Text = "Vehicle Name";
            // 
            // vehicleDescriptionText
            // 
            this.vehicleDescriptionText.Location = new System.Drawing.Point(154, 349);
            this.vehicleDescriptionText.Name = "vehicleDescriptionText";
            this.vehicleDescriptionText.Size = new System.Drawing.Size(147, 20);
            this.vehicleDescriptionText.TabIndex = 14;
            // 
            // vehicleNameText
            // 
            this.vehicleNameText.Location = new System.Drawing.Point(154, 320);
            this.vehicleNameText.Name = "vehicleNameText";
            this.vehicleNameText.Size = new System.Drawing.Size(147, 20);
            this.vehicleNameText.TabIndex = 15;
            // 
            // vehicleTypeText
            // 
            this.vehicleTypeText.Location = new System.Drawing.Point(154, 288);
            this.vehicleTypeText.Name = "vehicleTypeText";
            this.vehicleTypeText.Size = new System.Drawing.Size(147, 20);
            this.vehicleTypeText.TabIndex = 16;
            // 
            // operationButton
            // 
            this.operationButton.Location = new System.Drawing.Point(113, 403);
            this.operationButton.Name = "operationButton";
            this.operationButton.Size = new System.Drawing.Size(75, 23);
            this.operationButton.TabIndex = 17;
            this.operationButton.UseVisualStyleBackColor = true;
            this.operationButton.Click += new System.EventHandler(this.operationButton_Click);
            // 
            // Tesla_Automobiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 460);
            this.Controls.Add(this.operationButton);
            this.Controls.Add(this.vehicleTypeText);
            this.Controls.Add(this.vehicleNameText);
            this.Controls.Add(this.vehicleDescriptionText);
            this.Controls.Add(this.vehicleName);
            this.Controls.Add(this.vehicleDescription);
            this.Controls.Add(this.vehicleType);
            this.Controls.Add(this.dataView);
            this.Controls.Add(this.AddOption);
            this.Controls.Add(this.UpdateOption);
            this.Controls.Add(this.DeleteOption);
            this.Controls.Add(this.SearchOption);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Tesla_Automobiles";
            this.Text = "Tesla_Automobiles";
            this.Load += new System.EventHandler(this.Tesla_Automobiles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton SearchOption;
        private System.Windows.Forms.RadioButton DeleteOption;
        private System.Windows.Forms.RadioButton UpdateOption;
        private System.Windows.Forms.RadioButton AddOption;
        private System.Windows.Forms.DataGridView dataView;
        private TeslaDataSet teslaDataSet;
        private System.Windows.Forms.BindingSource vehicleDetailBindingSource;
        private TeslaDataSetTableAdapters.VehicleDetailTableAdapter vehicleDetailTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label vehicleType;
        private System.Windows.Forms.Label vehicleDescription;
        private System.Windows.Forms.Label vehicleName;
        private System.Windows.Forms.TextBox vehicleDescriptionText;
        private System.Windows.Forms.TextBox vehicleNameText;
        private System.Windows.Forms.TextBox vehicleTypeText;
        private System.Windows.Forms.Button operationButton;
    }
}