﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaDatabaseFirst_Assignment4_
{
    public partial class Tesla_Automobiles : Form
    {
        
        public Tesla_Automobiles()
        {
            InitializeComponent();
        }

        void Tesla_Automobiles_Load(object sender, EventArgs e)
        {
            try
            {
                this.vehicleDetailTableAdapter.Fill(this.teslaDataSet.VehicleDetail);
                DisableAllWidgets();
            }
            catch
            {
                MessageBox.Show("Unable to connect to database");
            }
        }

        void AddOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableAllWidgets();
                Reset();
                operationButton.Text = "ADD";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        void DeleteOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                operationButton.Text = "DELETE";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        void UpdateOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                operationButton.Text = "UPDATE";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        void SearchOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                operationButton.Text = "SEARCH";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void EnableAllWidgets() 
        {
            try
            {
                vehicleType.Visible = true;
                vehicleDescription.Visible = true;
                vehicleName.Visible = true;
                vehicleTypeText.Visible = true;
                vehicleNameText.Visible = true;
                vehicleDescriptionText.Visible = true;
                operationButton.Visible = true;
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void DisableAllWidgets()
        {
            try
            {
                vehicleType.Visible = false;
                vehicleDescription.Visible = false;
                vehicleName.Visible = false;
                vehicleTypeText.Visible = false;
                vehicleNameText.Visible = false;
                vehicleDescriptionText.Visible = false;
                operationButton.Visible = false;
            }
            catch
            {
                MessageBox.Show("Error");
            }

        }
        public void EnableVehicleNameWidget() 
        {
            try
            {
                vehicleType.Visible = false;
                vehicleDescription.Visible = false;
                vehicleName.Visible = true;
                vehicleTypeText.Visible = false;
                vehicleNameText.Visible = true;
                vehicleDescriptionText.Visible = false;
                operationButton.Visible = true;
            }
            catch 
            {
                MessageBox.Show("Error");
            }
        }

        private void operationButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch (operationButton.Text)
                { 
                    case "ADD" :
                        AddVehicle();
                        break;
                    case "DELETE":
                       DeleteVehicle();
                        break;
                    case "UPDATE":
                        UpdateVehicle();
                        break;
                    case "SEARCH":
                        SearchVehicle();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Error in connecting to database");
            }
        }
        public void AddVehicle() 
        {
            try
            {
                string name = vehicleNameText.Text;
                string description = vehicleDescriptionText.Text;
                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
                {
                    MessageBox.Show("Please provide all the details.");
                }
               
                else
                {
                    TeslaEntities teslaEntities = new TeslaEntities();
                    VehicleDetail vehicleDetails = new VehicleDetail();
                    vehicleDetails.VehicleId = Convert.ToInt32(vehicleTypeText.Text);
                    vehicleDetails.VehicleName = vehicleNameText.Text;
                    vehicleDetails.VehicleDescription = vehicleDescriptionText.Text;
                    teslaEntities.VehicleDetails.Add(vehicleDetails);
                    int response = teslaEntities.SaveChanges();
                    if (response > 0)
                    {
                        MessageBox.Show("Added Successfully");
                        Reset();
                        DisplayVehicles();
                    }
                    else
                        MessageBox.Show("Failed");
                }
            }
            catch
            {
                MessageBox.Show("Either the database connectivity is failed or Input is not in proper format");
            }
        }
        public void DeleteVehicle()
        {
            try
            {
             
                string vehicleName = vehicleNameText.Text;

                TeslaEntities teslaEntities = new TeslaEntities();

                var vehicleDetail = (from vehicle in teslaEntities.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();

                teslaEntities.VehicleDetails.Remove(vehicleDetail);
                int response = teslaEntities.SaveChanges();
                if (response > 0)
                {
                    MessageBox.Show("Deleted Successfully");
                  
                    Reset();
                    DisplayVehicles();
                  
                }
                else
                {
                    MessageBox.Show("Failed");
                   
                }
            }
            catch
            {
                MessageBox.Show("Not available");
                
            }
        }
        public void SearchVehicle() 
        {
            try
            {
                string vehicleName = vehicleNameText.Text;
                TeslaEntities teslaEntities = new TeslaEntities();
                var vehicleDetail = (from vehicle in teslaEntities.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();
                if (vehicleDetail != null)
                {
                    MessageBox.Show("Available");
                    Reset();
                }
                else
                    MessageBox.Show("Not available");
            }
            catch
            {
                MessageBox.Show("Not available");
            }
        }
        public void UpdateVehicle()
        {
            try
            {

                string vehicleName = vehicleNameText.Text;

                TeslaEntities teslaEntities = new TeslaEntities();

                var vehicleDetail = (from vehicle in teslaEntities.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();

                teslaEntities.VehicleDetails.Remove(vehicleDetail);
                int response = teslaEntities.SaveChanges();
                if (response > 0)
                {
                    EnableAllWidgets();
                    Reset();
                    operationButton.Text = "ADD";

                }
                else
                {
                    MessageBox.Show("Failed");

                }
            }
            catch 
            {
                MessageBox.Show("Not available");
            }
        }
        public void Reset() 
        {
            try
            {
                vehicleTypeText.Text = "";
                vehicleNameText.Text = "";
                vehicleDescriptionText.Text = "";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void DisplayVehicles() 
        {
            try
            {
                TeslaEntities teslaEntities = new TeslaEntities();
                dataView.DataSource = teslaEntities.VehicleDetails.ToList();
                teslaEntities.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
    }
}
