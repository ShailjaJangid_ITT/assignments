
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/11/2017 22:04:41
-- Generated from EDMX file: c:\users\shailja.jangid\documents\visual studio 2013\Projects\TeslaModelFirst(Assignment5)\TeslaModelFirst(Assignment5)\TeslaModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TeslaModel];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [VehicleType] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'VehicleDetails'
CREATE TABLE [dbo].[VehicleDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [VehicleName] nvarchar(max)  NOT NULL,
    [VehicleDescription] nvarchar(max)  NOT NULL,
    [VehicleId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleDetails'
ALTER TABLE [dbo].[VehicleDetails]
ADD CONSTRAINT [PK_VehicleDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [VehicleId] in table 'VehicleDetails'
ALTER TABLE [dbo].[VehicleDetails]
ADD CONSTRAINT [FK_VehicleDetailVehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleDetailVehicle'
CREATE INDEX [IX_FK_VehicleDetailVehicle]
ON [dbo].[VehicleDetails]
    ([VehicleId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------