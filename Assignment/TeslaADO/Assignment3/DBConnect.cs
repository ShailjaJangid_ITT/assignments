﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace Tesla
{
    class DBConnect
    {
        private MySqlConnection connection = null;
        private string server;
        private string database;
        private string uid;
        private string password;

        public DBConnect()
        {
            Initialize();
        }
        private void Initialize()
        {
            server = "localhost";
            database = "teslaautomobiles";
            uid = "root";
            password = "root";
            string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }
       
        public void OpenConnection()
        {
            try
            {
                connection.Open();

            }
            catch (Exception ex)
            {
                Console.WriteLine("    Unable to open connection.");
            }
        }
        public void CloseConnection()
        {
            try
            {
                connection.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("    Unable to close connection.");
            }
        }
        public void Insert(int vehicleType,string vehicleProperties)
        {
            try
            {
                int commaIndex = vehicleProperties.IndexOf(",");
                string vehicleName = vehicleProperties.Substring(0, commaIndex);
                string vehicleDescription = vehicleProperties.Substring(commaIndex + 1, vehicleProperties.Length - commaIndex - 1);
                string query = "INSERT INTO vehicleDetails VALUES('" + vehicleType + "','" + vehicleName + "','" + vehicleDescription + "')";
                MySqlCommand command = new MySqlCommand();
                command.CommandText = query;
                command.Connection = connection;
                this.OpenConnection();
                command.ExecuteNonQuery();
                this.CloseConnection();
            }
            catch (Exception ex) { 
                
            }
        }
        public int Search(int vehicleType,string vehicleName) {
            try
            {
                int flag = 0;
                string query = "SELECT vehicleID,vehicleName FROM vehicleDetails WHERE vehicleID =" + "'" + vehicleType + "'" + " AND vehicleName =" + "'" + vehicleName + "'";
                MySqlCommand command = new MySqlCommand();
                command.CommandText = query;
                command.Connection = connection;
                this.OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //for (int i = 0; i < reader.FieldCount; i++)
                    //    Console.WriteLine(reader.GetValue(i));
                    flag = 1;
                    break;
                }

                this.CloseConnection();
                return flag;
            }
            catch (Exception ex) {
                return 0;
            }
        }
        public int Update(int vehicleType,string vehicleName,string vehicleDescription,string vehicleToUpdate) {
            try
            {
                string query = "UPDATE vehicleDetails SET vehicleName=" + "'" + vehicleName + "'"
                    + "," + "vehicleDescription=" + "'" + vehicleDescription + "'" + "WHERE vehicleID=" + "'" + vehicleType + "' AND vehicleName=" +
                    "'" + vehicleToUpdate + "'";
                MySqlCommand command = new MySqlCommand();
                command.CommandText = query;
                command.Connection = connection;
                this.OpenConnection();
                command.ExecuteNonQuery();
                this.CloseConnection();
                return 1;
            }
            catch (Exception ex) {
                return 0;
            }
        }
        public int Delete(int vehicleType,string vehicleName) {
            try
            {
                string query = "DELETE FROM vehicleDetails WHERE vehicleID=" + "'" + vehicleType + "' AND vehicleName='" + vehicleName + "'";
                MySqlCommand command = new MySqlCommand();
                command.CommandText = query;
                command.Connection = connection;
                this.OpenConnection();
                command.ExecuteNonQuery();
                this.CloseConnection();
                return 1;
            }
            catch (Exception ex) {
                return 0;
            }
        }
        public void Display(int vehicleType) {
            try
            {
                string query = "SELECT vehicleName,vehicleDescription FROM vehicleDetails WHERE vehicleID='" + vehicleType + "'";
                MySqlCommand command = new MySqlCommand();
                command.CommandText = query;
                command.Connection = connection;
                this.OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                        Console.WriteLine("   " + reader.GetValue(i) + "\t\t" + reader.GetValue(++i));
                }
                this.CloseConnection();
            }
            catch (Exception e) {
                Console.WriteLine("    Unable to connect to database");
            }
        }
    }
}
