﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Tesla
{
    class Vehicle: VehicleOperations
    {
        DBConnect connect = new DBConnect();
        public void AddVehicle(string vehicleProperties,int vehicleType)
        {
                try
                {
                    connect.Insert(vehicleType, vehicleProperties);
                    Console.WriteLine("    Added Successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("    Unable to connect to database");
                }
        }
        public void DeleteVehicle(string vehicleName,int vehicleType)
        {
            try
            {
                int isDeleted = connect.Delete(vehicleType, vehicleName);
                if (isDeleted == 1)
                    Console.WriteLine("    Deleted Successfully.");
                else
                    Console.WriteLine("    Unable to delete.");
            }
            catch (Exception ex) {
                Console.WriteLine("    Unable to connect to database");
            }
          
        }
        public  int FindVehicle(string vehicleName, int vehicleType)
        {
            try
            {
                int flag=connect.Search(vehicleType,vehicleName);
                if (flag == 1)
                    Console.WriteLine("    The vehicle is available.");
                else
                    Console.WriteLine("    The vehicle you want to search is not available");
                return flag;
            }
            catch (Exception ex) {
                return 0;
            }
        }
        public  void UpdateVehicle(string vehicleToUpdate,int vehicleType) {
            try {
                int isUpdated = 0;
                int isAvailable=connect.Search(vehicleType,vehicleToUpdate);
                if (isAvailable == 1)
                {
                    Console.WriteLine("    Provide the new details in the order: vehicle name (then press enter) \n    and vehicle description:");
                    string name = Console.ReadLine();
                    string description = Console.ReadLine();

                   isUpdated=connect.Update(vehicleType,name,description,vehicleToUpdate);
                }
                if (isUpdated == 1)
                    Console.WriteLine("    Successfully updated");
            }
            catch (Exception ex) { 
            }
           
        }
        public void Type(int vehicleID) {
            try
            {
                string vehicleType = null;
                if (vehicleID == 1)
                    vehicleType = "Cars";
                else if (vehicleID == 2)
                    vehicleType = "Bicycles";
                else
                    vehicleType = "Trucks";
                Console.WriteLine("\t\t\tAvailable {0}", vehicleType);
            }
            catch (Exception ex) { 
            
            }
        }
        public void AllAvailableVehicles(int vehicleType)
        {
            try
            {
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Type(vehicleType);

                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Console.WriteLine("   Vehicle Name\t\tVehicle Description");
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                connect.Display(vehicleType);
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
            }
            catch (Exception ex) {
                Console.WriteLine("    Unable to connect to database");
            }
        }
        public void AvailableVehiclesofAllTypes()
        {
            try
            {
                int vehicleID = 1;
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Console.WriteLine("\t\t\tVehicles available in Tesla ");
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                while (vehicleID <= 3)
                {
                    AllAvailableVehicles(vehicleID);
                    vehicleID++;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("    Unable to connect to database");
            }
        }
    }
    
        
 }

