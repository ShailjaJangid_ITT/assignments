﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tesla
{
    class Tesla
    {
        static void Main(string[] args)
        {
            try
            {
                string isContinue;
                int vehicleType;
                Console.WriteLine("\t\t\t\tWelcme to Tesla");
                Console.WriteLine("   Vehicle categories available.");
                Console.WriteLine("     1. Car\n     2. Bicycle\n     3. Trucks\n     4. Show all available vehicles of all types");
                do
                {
                    Console.WriteLine("\n   Select the type of vehicle whose details you want to insert/modify(1-3)");
                    Vehicle vehicle = null;

                    int response = Convert.ToInt32(Console.ReadLine());
                    switch (response)
                    {
                        case 1:

                            vehicleType = 1;
                            Operations(vehicleType);
                            break;
                        case 2:
                            
                            vehicleType = 2;
                            Operations(vehicleType);
                            break;
                        case 3:
                            
                            vehicleType = 3;
                            Operations(vehicleType);
                            break;
                        case 4:
                            vehicle = new Vehicle();
                            vehicle.AvailableVehiclesofAllTypes();
                          
                            break;
                        default:
                            Console.WriteLine("\n   Wrong choice!");
                            break;
                    }


                    Console.WriteLine("\n   Do you want to continue with...");
                    Console.WriteLine("     Enter 'yes' to continue. ");
                    isContinue = Console.ReadLine();
                    isContinue = isContinue.ToLower();
                } while (isContinue.Equals("yes"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong Input Choice.");
            }
        }
        static public void Operations(int vehicleType)
        {
            try
            {

                Vehicle vehicle = new Vehicle();
                Console.WriteLine("   Operations available:");
                Console.WriteLine("     1. Adding new vehicle\n     2. Finding a vehicle\n     3. Updating existing vehicle details\n     4. Deleting a vehicle\n     5. List all avilable vehicles ");
                Console.WriteLine("\n   Specify your choice(1-5)");

                int choice = Convert.ToInt32(Console.ReadLine());
                string vehicleName;
                switch (choice)
                {

                    case 1:
                        string properties;
                        Console.WriteLine("   Provide the details in the order: vehicle ID, vehicle name, vehicle general description(separated by comma):");
                        properties = Console.ReadLine();
                        vehicle.AddVehicle(properties, vehicleType);
                        break;
                    case 2:

                        Console.WriteLine("   Enter the name of vehicle you want to search from list: ");
                        vehicleName = Console.ReadLine();
                        int check = vehicle.FindVehicle(vehicleName, vehicleType);
                        break;
                    case 3:
                        string VehicleToUpdate;
                        Console.WriteLine("   Enter the name of vehicle whose details you want to update:");
                        VehicleToUpdate = Console.ReadLine();
                        vehicle.UpdateVehicle(VehicleToUpdate, vehicleType);
                        break;
                    case 4:

                        Console.WriteLine("   Enter the name of vehicle you want to remove from list:");
                        vehicleName = Console.ReadLine();
                        vehicle.DeleteVehicle(vehicleName, vehicleType);
                        break;
                    case 5:
                        vehicle.AllAvailableVehicles(vehicleType);
                        break;

                    default:
                        Console.WriteLine("Wrong choice!");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong Choice");
            }
        }

    }
}
