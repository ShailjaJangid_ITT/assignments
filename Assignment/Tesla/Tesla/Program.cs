﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Tesla
{
    class Vehicle: VehicleOperations
    {
        public void AddVehicle(string vehicleProperties,int vehicleType)
        {
                try
                {
                    vehicleProperties = vehicleType +" "+ vehicleProperties;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    StreamWriter streamWriter = new StreamWriter("D:\\Vehicles.txt", true);

                    streamWriter.WriteLine(vehicleProperties);
                    streamWriter.Close();
                    Console.WriteLine("    Added Successfully.");
                }
                catch (Exception e)
                {
                    Console.WriteLine("    Unable to open file. ");
                }
        }
        public void DeleteVehicle(string vehicleName,int vehicleType)
        {
            int i,j;
             try
                {
                    int IfAvail = FindVehicle(vehicleName,vehicleType);
                    if (IfAvail == 1)
                    {
                         string[] AllVehicles;
                         string[] Details = null; 
               
                         Details = File.ReadAllLines("D:\\Vehicles.txt");

                         for (i = 0; i < Details.Length; i++)
                         {
                                int space = Details[i].IndexOf(" ");
                                int vehicleID = int.Parse(Details[i].Substring(0, space));

                                if (vehicleID == vehicleType)
                                {
                                    Details[i] = Details[i].Substring(space + 1, Details[i].Length - space - 1);
                                    int CommaIndex = Details[i].IndexOf(",");
                                    string id = Details[i].Substring(0, CommaIndex);
                                    string data = Details[i].Substring(CommaIndex + 1, Details[i].Length - CommaIndex - 1);
                                    int SecondCommaIndex = data.IndexOf(",");
                                    data = data.Substring(0, SecondCommaIndex);
                                    bool IsEqual = String.Equals(data, vehicleName, StringComparison.Ordinal);
                                    if (IsEqual)
                                    {
                                        Details[i] = "";
                                        break;
                                    }
                                }
                          }
                          for (j = i; j < Details.Length - 1; j++)
                          {
                                Details[j] = Details[j + 1];
                          }
                          Details[j] = "";
                          AllVehicles = new string[Details.Length - 1];
                          for (int k = 0; k < AllVehicles.Length; k++) {
                          AllVehicles[k] = Details[k];
                          }
               
                          GC.Collect();
                          GC.WaitForPendingFinalizers();
                          File.WriteAllLines("D:\\Vehicles.txt", AllVehicles);
                          Console.WriteLine("    Deleted Successfully.");
                
                         }
                        else
                            return;
                     }
                     catch (Exception e)
                     {
                         Console.WriteLine("    Error in opening file.");
                     }
        }
        public  int FindVehicle(string vehicleName, int vehicleType)
        {
            string data;
            StreamReader streamReader = null;
            int flag = 0;
            try
            {
                streamReader = new StreamReader("D:\\Vehicles.txt");
            }
            catch (Exception e) {
                Console.WriteLine("    Error in opening file");
            }
            try
            {
                while ((data = streamReader.ReadLine()) != null)
                {
                    int space = data.IndexOf(" ");
                    int vehicleID = int.Parse(data.Substring(0, space));                 
                    if (vehicleType == vehicleID)
                    {
                        int CommaIndex = data.IndexOf(",");
                        data = data.Substring(CommaIndex + 1, data.Length - CommaIndex - 1);
                        int SecondCommaIndex = data.IndexOf(",");
                        data = data.Substring(0, SecondCommaIndex);
                        bool IsEqual = String.Equals(data, vehicleName, StringComparison.Ordinal);
                        if (IsEqual)
                        {
                            flag = 1;
                            Console.WriteLine("    The vehicle is available.");
                            return flag;
                        }
                        else
                        {
                            flag = 0;
                            continue;
                        }
                    }
                }
                if (flag == 0)
                    Console.WriteLine("     The vehicle you want to search is not available.");
                return flag;
            }
            catch (Exception e)
            {
                return 0;
            }
           

        }
        public  void UpdateVehicle(string vehicleToUpdate,int vehicleType) {
            try
            {
                int AvailLine = FindVehicle(vehicleToUpdate, vehicleType);
                if (AvailLine > 0)
                {
                    string[] Details = null;
                    string name, description;
                    Console.WriteLine("    Provide the new details in the order: vehicle name (then press enter) \n    and vehicle description:");
                    name = Console.ReadLine();
                    description = Console.ReadLine();

                    Details = File.ReadAllLines("D:\\Vehicles.txt");

                    for (int i = 0; i < Details.Length; i++)
                    {
                        int space = Details[i].IndexOf(" ");
                        int vehicleID = int.Parse(Details[i].Substring(0, space));
                        
                        if (vehicleID == vehicleType)
                        {
                            Details[i] = Details[i].Substring(space + 1, Details[i].Length - space - 1);
                            int CommaIndex = Details[i].IndexOf(",");
                            string id = Details[i].Substring(0, CommaIndex);
                            string data = Details[i].Substring(CommaIndex + 1, Details[i].Length - CommaIndex - 1);
                            int SecondCommaIndex = data.IndexOf(",");
                            data = data.Substring(0, SecondCommaIndex);
                            bool IsEqual = String.Equals(data, vehicleToUpdate, StringComparison.Ordinal);
                            if (IsEqual)
                            {
                                Details[i] = vehicleType + " " + id + "," + name + "," + description;
                                break;
                            }
                        }
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    File.WriteAllLines("D:\\Vehicles.txt", Details);
                    Console.WriteLine("    Updated Successfully.");
                }


                else
                    return;
            }
            catch (Exception e)
            {
                Console.WriteLine("    Error in opening file. ");

            }
        }
        public void Type(int vehicleID) {
            try
            {
                string vehicleType = null;
                if (vehicleID == 1)
                    vehicleType = "Cars";
                else if (vehicleID == 2)
                    vehicleType = "Bicycles";
                else
                    vehicleType = "Trucks";
                Console.WriteLine("\t\t\tAvailable {0}", vehicleType);
            }
            catch (Exception e) { 
            
            }
        }
        public void AllAvailableVehicles(int vehicleType)
        {
            try
            {
                StreamReader streamReader = null;
                string[] Available = null;
                string data;

                Available = File.ReadAllLines("D:\\Vehicles.txt");
                streamReader = new StreamReader("D:\\Vehicles.txt");


                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Type(vehicleType);
               
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Console.WriteLine("   Vehicle ID\t\tVehicle Name\t\tVehicle Description");
                for (int i = 0; i < 80; i++)
                    Console.Write("-");

                while ((data = streamReader.ReadLine()) != null)
                {
                    int space = data.IndexOf(" ");
                    int vehicleID = int.Parse(data.Substring(0,space));
                    if(vehicleType==vehicleID){
                    data = data.Substring(space+1,data.Length-space-1);
                    string id, name, description;
                    int index = data.IndexOf(",");
                    id = data.Substring(0, index);
                    data = data.Substring(index + 1, data.Length - index - 1);
                    int SecondIndex = data.IndexOf(",");
                    name = data.Substring(0, SecondIndex);
                    description = data.Substring(SecondIndex + 1, data.Length - SecondIndex - 1);
                    Console.WriteLine("        {0}\t\t{1}\t\t\t{2}", id, name, description);
                }

                }
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
            }
            catch (Exception e)
            {
                Console.WriteLine("    Unable to open file.");
            }
        }
        public void AvailableVehiclesofAllTypes()
        {
            int vehicleID = 1;
            for (int i = 0; i < 80; i++)
                Console.Write("-");
            Console.WriteLine("\t\t\tVehicles available in Tesla ");
            for (int i = 0; i < 80; i++)
                Console.Write("-");
            while (vehicleID <= 3)
            {
                AllAvailableVehicles(vehicleID);
                vehicleID++;
            }

        }
    }
    
        
 }

