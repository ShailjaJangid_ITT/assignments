﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tesla
{
    interface VehicleOperations
    {
        void AddVehicle(string VehicleProperties, int vehicleType);
        void DeleteVehicle(string vehicleName, int vehicleType);
        int FindVehicle(string vehicleName, int vehicleType);
        void UpdateVehicle(string vehicleToUpdate, int vehicleType);
        void AllAvailableVehicles(int vehicleType);
        void AvailableVehiclesofAllTypes();

    }
}
