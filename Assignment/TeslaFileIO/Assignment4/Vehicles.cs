﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Tesla
{
    class Vehicle : VehicleOperations
    {
        int vehicleType;
        string vehicleProperties;

        public Vehicle()
        {

        }
        public Vehicle(int vehicleType)
        {
            this.vehicleType = vehicleType;
        }
        public Vehicle(int vehicleType, string vehicleName)
        {
            this.vehicleType = vehicleType;
            this.vehicleProperties = vehicleName;
        }
        public void AddVehicle()
        {
            try
            {
                vehicleProperties = vehicleType + " " + vehicleProperties;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                StreamWriter streamWriter = new StreamWriter("F:\\Vehicles.txt", true);

                streamWriter.WriteLine(vehicleProperties);
                Console.WriteLine("    Added Successfully.");
                streamWriter.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("    Unable to open file. ");
            }
        }
        public void DeleteVehicle()
        {
           
            try
            {
                int i, j;
                int isAvailble = FindVehicle(vehicleProperties, vehicleType);
                if (isAvailble == 1)
                {
                    string[] allVehicles;
                    string[] details = null;

                    details = File.ReadAllLines("F:\\Vehicles.txt");

                    for (i = 0; i < details.Length; i++)
                    {
                        int space = details[i].IndexOf(" ");
                        int vehicleID = int.Parse(details[i].Substring(0, space));

                        if (vehicleID == vehicleType)
                        {
                            details[i] = details[i].Substring(space + 1, details[i].Length - space - 1);
                            int commaIndex = details[i].IndexOf(",");
                            string id = details[i].Substring(0, commaIndex);
                            string data = details[i].Substring(commaIndex + 1, details[i].Length - commaIndex - 1);
                            int secondCommaIndex = data.IndexOf(",");
                            data = data.Substring(0, secondCommaIndex);
                            bool isEqual = String.Equals(data, vehicleProperties, StringComparison.Ordinal);
                            if (isEqual)
                            {
                                details[i] = "";
                                break;
                            }
                        }
                    }
                    for (j = i; j < details.Length - 1; j++)
                    {
                        details[j] = details[j + 1];
                    }
                    details[j] = "";
                    allVehicles = new string[details.Length - 1];
                    for (int k = 0; k < allVehicles.Length; k++)
                    {
                        allVehicles[k] = details[k];
                    }

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    File.WriteAllLines("F:\\Vehicles.txt", allVehicles);
                    Console.WriteLine("    Deleted Successfully.");

                }
                else
                    return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("    Error in opening file.");
            }
        }
        public int FindVehicle(string vehicleName, int vehicleType)
        {
            try
            {
                string data;
                int flag = 0;
                StreamReader streamReader = null;
                streamReader = new StreamReader("F:\\Vehicles.txt");
                while ((data = streamReader.ReadLine()) != null)
                {
                    int space = data.IndexOf(" ");
                    int vehicleID = int.Parse(data.Substring(0, space));
                    if (vehicleType == vehicleID)
                    {
                        int commaIndex = data.IndexOf(",");
                        data = data.Substring(commaIndex + 1, data.Length - commaIndex - 1);
                        int secondCommaIndex = data.IndexOf(",");
                        data = data.Substring(0, secondCommaIndex);
                        bool isEqual = String.Equals(data, vehicleName, StringComparison.Ordinal);
                        if (isEqual)
                        {
                            flag = 1;
                            Console.WriteLine("    The vehicle is available.");
                            return flag;
                        }
                        else
                        {
                            flag = 0;
                            continue;
                        }
                    }
                }
                if (flag == 0)
                    Console.WriteLine("     The vehicle you want to search is not available.");
                return flag;
            }
            catch (Exception ex)
            {
                Console.WriteLine("    Error in opening file");
                return 0;
            }


        }
        public void UpdateVehicle()
        {
            try
            {
                int isAvailable = FindVehicle(vehicleProperties, vehicleType);
                if (isAvailable > 0)
                {
                    string[] details = null;
                    string name, description;
                    Console.WriteLine("    Provide the new details in the order: vehicle name (then press enter) \n    and vehicle description:");
                    name = Console.ReadLine();
                    description = Console.ReadLine();

                    details = File.ReadAllLines("F:\\Vehicles.txt");

                    for (int i = 0; i < details.Length; i++)
                    {
                        int space = details[i].IndexOf(" ");
                        int vehicleID = int.Parse(details[i].Substring(0, space));

                        if (vehicleID == vehicleType)
                        {
                            details[i] = details[i].Substring(space + 1, details[i].Length - space - 1);
                            int commaIndex = details[i].IndexOf(",");
                            string id = details[i].Substring(0, commaIndex);
                            string data = details[i].Substring(commaIndex + 1, details[i].Length - commaIndex - 1);
                            int secondCommaIndex = data.IndexOf(",");
                            data = data.Substring(0, secondCommaIndex);
                            bool isEqual = String.Equals(data, vehicleProperties, StringComparison.Ordinal);
                            if (isEqual)
                            {
                                details[i] = vehicleType + " " + id + "," + name + "," + description;
                                break;
                            }
                        }
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    File.WriteAllLines("F:\\Vehicles.txt", details);
                    Console.WriteLine("    Updated Successfully.");
                }


                else
                    return;
            }
            catch (Exception ex)
            {
                Console.WriteLine("    Error in opening file. ");

            }
        }
        public void Type(int vehicleID)
        {
            try
            {
                string vehicleType = null;
                if (vehicleID == 1)
                    vehicleType = "Cars";
                else if (vehicleID == 2)
                    vehicleType = "Bicycles";
                else
                    vehicleType = "Trucks";
                Console.WriteLine("\t\t\tAvailable {0}", vehicleType);
            }
            catch (Exception ex)
            {

            }
        }
        public void AllAvailableVehicles()
        {
            try
            {
                StreamReader streamReader = null;
                string[] available = null;
                string data;

                available = File.ReadAllLines("F:\\Vehicles.txt");
                streamReader = new StreamReader("F:\\Vehicles.txt");


                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Type(vehicleType);

                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Console.WriteLine("   Vehicle ID\t\tVehicle Name\t\tVehicle Description");
                for (int i = 0; i < 80; i++)
                    Console.Write("-");

                while ((data = streamReader.ReadLine()) != null)
                {
                    int space = data.IndexOf(" ");
                    int vehicleID = int.Parse(data.Substring(0, space));
                    if (vehicleType == vehicleID)
                    {
                        data = data.Substring(space + 1, data.Length - space - 1);
                        string id, name, description;
                        int index = data.IndexOf(",");
                        id = data.Substring(0, index);
                        data = data.Substring(index + 1, data.Length - index - 1);
                        int secondIndex = data.IndexOf(",");
                        name = data.Substring(0, secondIndex);
                        description = data.Substring(secondIndex + 1, data.Length - secondIndex - 1);
                        Console.WriteLine("        {0}\t\t{1}\t\t\t{2}", id, name, description);
                    }

                }
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
            }
            catch (Exception ex)
            {
                Console.WriteLine("    Unable to open file.");
            }
        }
        public void AvailableVehiclesofAllTypes()
        {
            try
            {
                //int vehicleType = 1;
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                Console.WriteLine("\t\t\tVehicles available in Tesla ");
                for (int i = 0; i < 80; i++)
                    Console.Write("-");
                while (vehicleType <= 3)
                {
                    AllAvailableVehicles();
                    vehicleType++;
                }

            }
            catch (Exception ex) {
                Console.WriteLine("    Error in opening file.");
            }
        }
    }


}

