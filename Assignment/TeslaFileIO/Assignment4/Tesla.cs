﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Tesla
{
    class Tesla
    {
        static void Main(string[] args)
        {
            try
            {
                string isContinue;
                int vehicleType;
                Console.WriteLine("\n\t\t\t\tWelcme to Tesla");
                Console.WriteLine("   Vehicle categories available.");
                Console.WriteLine("     1. Car\n     2. Bicycle\n     3. Trucks\n     4. Show all available vehicles of all types");
                do
                {
                    Console.WriteLine("\n   Select the type of vehicle whose details you want to insert/modify(1-3)");
                    Vehicle vehicle = null;

                    int response = Convert.ToInt32(Console.ReadLine());
                    switch (response){
                        case 1:

                            vehicleType = 1;
                            Operations(vehicleType);
                    
                            break;
                        case 2:
                            
                            vehicleType = 2;
                            Operations(vehicleType);
                            break;
                        case 3:
                            
                            vehicleType = 3;
                            Operations(vehicleType);
                            break;
                        case 4:
                            vehicleType = 1;
                            vehicle = new Vehicle(vehicleType);
                            Thread thread = new Thread(new ThreadStart(vehicle.AvailableVehiclesofAllTypes));
                            thread.Start();
                            thread.Join();
                            break;
                        default:
                            Console.WriteLine("\n   Wrong choice!");
                            break;
                    }


                    Console.WriteLine("\n   Do you want to continue with...");
                    Console.WriteLine("     Enter 'yes' to continue. ");
                    isContinue = Console.ReadLine();
                    isContinue = isContinue.ToLower();
                } while (isContinue.Equals("yes"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Wrong Input Choice.");
            }
        }
        static public void Operations(int vehicleType)
        {
            try
            {
                Thread thread = null;
                Vehicle vehicle = null;
                Console.WriteLine("   Operations available:");
                Console.WriteLine("     1. Adding new vehicle\n     2. Finding a vehicle\n     3. Updating existing vehicle details\n     4. Deleting a vehicle\n     5. List all avilable vehicles ");
                Console.WriteLine("\n   Specify your choice(1-5)");

                int choice = Convert.ToInt32(Console.ReadLine());
                string vehicleName;
                switch (choice)
                {       
                    case 1:
                        string properties;
                        Console.WriteLine("   Provide the details in the order: vehicle ID, vehicle name, vehicle general description(separated by comma):");
                        properties = Console.ReadLine();
                        vehicle = new Vehicle(vehicleType,properties);
                        thread = new Thread(new ThreadStart(vehicle.AddVehicle));
                        thread.Start();
                        thread.Join();
                        break;
                    case 2:
                        vehicle = new Vehicle();
                        int check=0;
                        Console.WriteLine("   Enter the name of vehicle you want to search from list: ");
                        vehicleName = Console.ReadLine();
                        thread = new Thread(() => { check = vehicle.FindVehicle(vehicleName,vehicleType); });
                        thread.Start();
                        thread.Join();
                        break;
                    case 3:
                        string vehicleToUpdate;
                        Console.WriteLine("   Enter the name of vehicle whose details you want to update:");
                        vehicleToUpdate = Console.ReadLine();
                        vehicle = new Vehicle(vehicleType,vehicleToUpdate);
                        thread = new Thread(new ThreadStart(vehicle.UpdateVehicle));
                        thread.Start();
                        thread.Join();
                        break;
                    case 4:
                        Console.WriteLine("   Enter the name of vehicle you want to remove from list:");
                        vehicleName = Console.ReadLine();
                        vehicle = new Vehicle(vehicleType, vehicleName);
                        thread = new Thread(new ThreadStart(vehicle.DeleteVehicle));
                        thread.Start();
                        thread.Join();
                        break;
                    case 5:
                        vehicle = new Vehicle(vehicleType);
                        thread = new Thread(new ThreadStart(vehicle.AllAvailableVehicles));
                        thread.Start();
                        thread.Join();
                        break;

                    default:
                        Console.WriteLine("Wrong choice!");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Wrong Choice");
            }
        }

    }
}
