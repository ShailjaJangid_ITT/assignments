﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tesla
{
    interface VehicleOperations
    {
        void AddVehicle();
        void DeleteVehicle();
        int FindVehicle(string vehicleName, int vehicleType);
        void UpdateVehicle();
        void AllAvailableVehicles();
        void AvailableVehiclesofAllTypes();

    }
}
