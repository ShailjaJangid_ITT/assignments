﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaCodeFirst_Assignment6_
{
    public partial class TeslaAutomobiles : Form
    {
        public TeslaAutomobiles()
        {
            InitializeComponent();
        }

        private void TeslaAutomobiles_Load(object sender, EventArgs e)
        {
            try
            {
                this.vehicleDetailTableAdapter.Fill(this.teslaDataSet.VehicleDetail);
                DisableAllWidgets();
            }
            catch
            {
                MessageBox.Show("Unable to connect to database");
            }

            this.vehicleDetailTableAdapter.Fill(this.teslaDataSet.VehicleDetail);

        }

        private void AddOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableAllWidgets();
                Reset();
                OperationButton.Text = "ADD";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void DeleteOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                OperationButton.Text = "DELETE";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void UpdateOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                OperationButton.Text = "DELETE";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void SearchOption_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                EnableVehicleNameWidget();
                Reset();
                OperationButton.Text = "SEARCH";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void EnableAllWidgets()
        {
            try
            {
                VehicleType.Visible = true;
                VehicleDescription.Visible = true;
                VehicleName.Visible = true;
                VehicleTypeText.Visible = true;
                VehicleNameText.Visible = true;
                VehicleDescriptionText.Visible = true;
                OperationButton.Visible = true;
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void DisableAllWidgets()
        {
            try
            {
                VehicleType.Visible = false;
                VehicleDescription.Visible = false;
                VehicleName.Visible = false;
                VehicleTypeText.Visible = false;
                VehicleNameText.Visible = false;
                VehicleDescriptionText.Visible = false;
                OperationButton.Visible = false;
            }
            catch
            {
                MessageBox.Show("Error");
            }

        }
        public void EnableVehicleNameWidget()
        {
            try
            {
                VehicleType.Visible = false;
                VehicleDescription.Visible = false;
                VehicleName.Visible = true;
                VehicleTypeText.Visible = false;
                VehicleNameText.Visible = true;
                VehicleDescriptionText.Visible = false;
                OperationButton.Visible = true;
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        public void Reset()
        {
            try
            {
                VehicleTypeText.Text = "";
                VehicleNameText.Text = "";
                VehicleDescriptionText.Text = "";
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void OperationButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch (OperationButton.Text)
                {
                    case "ADD":
                        AddVehicle();
                        break;
                    case "DELETE":
                        DeleteVehicle();
                        break;
                    case "UPDATE":
                        UpdateVehicle();
                        break;
                    case "SEARCH":
                        SearchVehicle();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Error in connecting to database");
            }
        }
        public void AddVehicle()
        {
            try
            {
                string name = VehicleNameText.Text;
                string description = VehicleDescriptionText.Text;
                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
                {
                    MessageBox.Show("Please provide all the details.");
                }

                else
                {
                    ModelTesla teslaContainer = new ModelTesla();
                    VehicleDetail vehicleDetails = new VehicleDetail();
                    vehicleDetails.VehicleId = Convert.ToInt32(VehicleTypeText.Text);
                    vehicleDetails.VehicleName = VehicleNameText.Text;
                    vehicleDetails.VehicleDescription = VehicleDescriptionText.Text;
                    teslaContainer.VehicleDetails.Add(vehicleDetails);
                    int response = teslaContainer.SaveChanges();
                    if (response > 0)
                    {
                        MessageBox.Show("Added Successfully");
                        Reset();
                        DisplayVehicles();
                    }
                    else
                        MessageBox.Show("Failed");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                //MessageBox.Show("Either the database connectivity is failed or Input is not in proper format");
            }
        }
        public void DeleteVehicle()
        {
            try
            {

                string vehicleName = VehicleNameText.Text;

                ModelTesla teslaContainer = new ModelTesla();

                var vehicleDetail = (from vehicle in teslaContainer.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();

                teslaContainer.VehicleDetails.Remove(vehicleDetail);
                int response = teslaContainer.SaveChanges();
                if (response > 0)
                {
                    MessageBox.Show("Deleted Successfully");

                    Reset();
                    DisplayVehicles();

                }
                else
                {
                    MessageBox.Show("Failed");

                }
            }
            catch
            {
                MessageBox.Show("Not available");

            }
        }
        public void SearchVehicle()
        {
            try
            {
                string vehicleName = VehicleNameText.Text;
                ModelTesla teslaContainer = new ModelTesla();
                var vehicleDetail = (from vehicle in teslaContainer.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();
                if (vehicleDetail != null)
                {
                    MessageBox.Show("Available");
                    Reset();
                }
                else
                    MessageBox.Show("Not available");
            }
            catch
            {
                MessageBox.Show("Not available");
            }
        }
        public void UpdateVehicle()
        {
            try
            {

                string vehicleName = VehicleNameText.Text;

                ModelTesla teslaContainer = new ModelTesla();
                var vehicleDetail = (from vehicle in teslaContainer.VehicleDetails where vehicle.VehicleName == vehicleName select vehicle).Single();

                teslaContainer.VehicleDetails.Remove(vehicleDetail);
                int response = teslaContainer.SaveChanges();
                if (response > 0)
                {
                    EnableAllWidgets();
                    Reset();
                    OperationButton.Text = "ADD";

                }
                else
                {
                    MessageBox.Show("Failed");

                }
            }
            catch
            {
                MessageBox.Show("Not available");
            }
        }
        public void DisplayVehicles()
        {
            try
            {
                ModelTesla teslaContainer = new ModelTesla();
                dataView.DataSource = teslaContainer.VehicleDetails.ToList();
                teslaContainer.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

    }
}
