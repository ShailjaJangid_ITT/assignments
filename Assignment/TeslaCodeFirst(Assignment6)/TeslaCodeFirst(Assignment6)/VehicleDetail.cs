﻿namespace TeslaCodeFirst_Assignment6_
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleDetail")]
    public partial class VehicleDetail
    {
        public int ID { get; set; }

    
        public string VehicleName { get; set; }

       
        public string VehicleDescription { get; set; }

        public int VehicleId { get; set; }

        public virtual Vehicles Vehicle { get; set; }
    }
}
