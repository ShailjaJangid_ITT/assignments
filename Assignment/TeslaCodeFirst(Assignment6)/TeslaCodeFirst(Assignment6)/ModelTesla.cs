namespace TeslaCodeFirst_Assignment6_
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelTesla : DbContext
    {
        public ModelTesla()
            : base("name=ModelTesla")
        {
        }

        public virtual DbSet<VehicleDetail> VehicleDetails { get; set; }
        public virtual DbSet<Vehicles> Vehicles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VehicleDetail>()
                .Property(e => e.VehicleName)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleDetail>()
                .Property(e => e.VehicleDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Vehicles>()
                .HasMany(e => e.VehicleDetails)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);
        }
    }
}
