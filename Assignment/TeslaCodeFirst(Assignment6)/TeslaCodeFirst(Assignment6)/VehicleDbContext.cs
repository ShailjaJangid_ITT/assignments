﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace TeslaCodeFirst_Assignment6_
{
    class VehicleDbContext:DbContext
    {
        public DbSet<Vehicles> Vehicles { get; set; }
        public DbSet<VehicleDetail> VehicleDetails { get; set; }
    }
}
