﻿namespace TeslaCodeFirst_Assignment6_
{
    partial class TeslaAutomobiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SearchOption = new System.Windows.Forms.RadioButton();
            this.DeleteOption = new System.Windows.Forms.RadioButton();
            this.UpdateOption = new System.Windows.Forms.RadioButton();
            this.AddOption = new System.Windows.Forms.RadioButton();
            this.VehicleDescription = new System.Windows.Forms.Label();
            this.VehicleName = new System.Windows.Forms.Label();
            this.VehicleType = new System.Windows.Forms.Label();
            this.VehicleDescriptionText = new System.Windows.Forms.TextBox();
            this.VehicleNameText = new System.Windows.Forms.TextBox();
            this.VehicleTypeText = new System.Windows.Forms.TextBox();
            this.OperationButton = new System.Windows.Forms.Button();
            this.dataView = new System.Windows.Forms.DataGridView();
            this.teslaDataSet = new TeslaCodeFirst_Assignment6_.TeslaDataSet();
            this.vehicleDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleDetailTableAdapter = new TeslaCodeFirst_Assignment6_.TeslaDataSetTableAdapters.VehicleDetailTableAdapter();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vehicle Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cars : 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Trucks : 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Buses : 3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Operations Available";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(321, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Welcome to Tesla";
            // 
            // SearchOption
            // 
            this.SearchOption.AutoSize = true;
            this.SearchOption.Location = new System.Drawing.Point(168, 219);
            this.SearchOption.Name = "SearchOption";
            this.SearchOption.Size = new System.Drawing.Size(59, 17);
            this.SearchOption.TabIndex = 7;
            this.SearchOption.TabStop = true;
            this.SearchOption.Text = "Search";
            this.SearchOption.UseVisualStyleBackColor = true;
            this.SearchOption.CheckedChanged += new System.EventHandler(this.SearchOption_CheckedChanged);
            // 
            // DeleteOption
            // 
            this.DeleteOption.AutoSize = true;
            this.DeleteOption.Location = new System.Drawing.Point(168, 196);
            this.DeleteOption.Name = "DeleteOption";
            this.DeleteOption.Size = new System.Drawing.Size(56, 17);
            this.DeleteOption.TabIndex = 8;
            this.DeleteOption.TabStop = true;
            this.DeleteOption.Text = "Delete";
            this.DeleteOption.UseVisualStyleBackColor = true;
            this.DeleteOption.CheckedChanged += new System.EventHandler(this.DeleteOption_CheckedChanged);
            // 
            // UpdateOption
            // 
            this.UpdateOption.AutoSize = true;
            this.UpdateOption.Location = new System.Drawing.Point(31, 219);
            this.UpdateOption.Name = "UpdateOption";
            this.UpdateOption.Size = new System.Drawing.Size(60, 17);
            this.UpdateOption.TabIndex = 9;
            this.UpdateOption.TabStop = true;
            this.UpdateOption.Text = "Update";
            this.UpdateOption.UseVisualStyleBackColor = true;
            this.UpdateOption.CheckedChanged += new System.EventHandler(this.UpdateOption_CheckedChanged);
            // 
            // AddOption
            // 
            this.AddOption.AutoSize = true;
            this.AddOption.Location = new System.Drawing.Point(31, 196);
            this.AddOption.Name = "AddOption";
            this.AddOption.Size = new System.Drawing.Size(44, 17);
            this.AddOption.TabIndex = 10;
            this.AddOption.TabStop = true;
            this.AddOption.Text = "Add";
            this.AddOption.UseVisualStyleBackColor = true;
            this.AddOption.CheckedChanged += new System.EventHandler(this.AddOption_CheckedChanged);
            // 
            // VehicleDescription
            // 
            this.VehicleDescription.AutoSize = true;
            this.VehicleDescription.Location = new System.Drawing.Point(32, 310);
            this.VehicleDescription.Name = "VehicleDescription";
            this.VehicleDescription.Size = new System.Drawing.Size(98, 13);
            this.VehicleDescription.TabIndex = 11;
            this.VehicleDescription.Text = "Vehicle Description";
            // 
            // VehicleName
            // 
            this.VehicleName.AutoSize = true;
            this.VehicleName.Location = new System.Drawing.Point(32, 284);
            this.VehicleName.Name = "VehicleName";
            this.VehicleName.Size = new System.Drawing.Size(73, 13);
            this.VehicleName.TabIndex = 12;
            this.VehicleName.Text = "Vehicle Name";
            // 
            // VehicleType
            // 
            this.VehicleType.AutoSize = true;
            this.VehicleType.Location = new System.Drawing.Point(32, 258);
            this.VehicleType.Name = "VehicleType";
            this.VehicleType.Size = new System.Drawing.Size(69, 13);
            this.VehicleType.TabIndex = 13;
            this.VehicleType.Text = "Vehicle Type";
            // 
            // VehicleDescriptionText
            // 
            this.VehicleDescriptionText.Location = new System.Drawing.Point(147, 307);
            this.VehicleDescriptionText.Name = "VehicleDescriptionText";
            this.VehicleDescriptionText.Size = new System.Drawing.Size(161, 20);
            this.VehicleDescriptionText.TabIndex = 14;
            // 
            // VehicleNameText
            // 
            this.VehicleNameText.Location = new System.Drawing.Point(147, 277);
            this.VehicleNameText.Name = "VehicleNameText";
            this.VehicleNameText.Size = new System.Drawing.Size(161, 20);
            this.VehicleNameText.TabIndex = 15;
            // 
            // VehicleTypeText
            // 
            this.VehicleTypeText.Location = new System.Drawing.Point(147, 251);
            this.VehicleTypeText.Name = "VehicleTypeText";
            this.VehicleTypeText.Size = new System.Drawing.Size(161, 20);
            this.VehicleTypeText.TabIndex = 16;
            // 
            // OperationButton
            // 
            this.OperationButton.Location = new System.Drawing.Point(128, 354);
            this.OperationButton.Name = "OperationButton";
            this.OperationButton.Size = new System.Drawing.Size(75, 23);
            this.OperationButton.TabIndex = 17;
            this.OperationButton.UseVisualStyleBackColor = true;
            this.OperationButton.Click += new System.EventHandler(this.OperationButton_Click);
            // 
            // dataView
            // 
            this.dataView.AutoGenerateColumns = false;
            this.dataView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.vehicleNameDataGridViewTextBoxColumn,
            this.vehicleDescriptionDataGridViewTextBoxColumn,
            this.vehicleIdDataGridViewTextBoxColumn});
            this.dataView.DataSource = this.vehicleDetailBindingSource;
            this.dataView.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataView.Location = new System.Drawing.Point(343, 32);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(419, 355);
            this.dataView.TabIndex = 18;
            // 
            // teslaDataSet
            // 
            this.teslaDataSet.DataSetName = "TeslaDataSet";
            this.teslaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vehicleDetailBindingSource
            // 
            this.vehicleDetailBindingSource.DataMember = "VehicleDetail";
            this.vehicleDetailBindingSource.DataSource = this.teslaDataSet;
            // 
            // vehicleDetailTableAdapter
            // 
            this.vehicleDetailTableAdapter.ClearBeforeFill = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehicleNameDataGridViewTextBoxColumn
            // 
            this.vehicleNameDataGridViewTextBoxColumn.DataPropertyName = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.HeaderText = "VehicleName";
            this.vehicleNameDataGridViewTextBoxColumn.Name = "vehicleNameDataGridViewTextBoxColumn";
            // 
            // vehicleDescriptionDataGridViewTextBoxColumn
            // 
            this.vehicleDescriptionDataGridViewTextBoxColumn.DataPropertyName = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.HeaderText = "VehicleDescription";
            this.vehicleDescriptionDataGridViewTextBoxColumn.Name = "vehicleDescriptionDataGridViewTextBoxColumn";
            // 
            // vehicleIdDataGridViewTextBoxColumn
            // 
            this.vehicleIdDataGridViewTextBoxColumn.DataPropertyName = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.HeaderText = "VehicleId";
            this.vehicleIdDataGridViewTextBoxColumn.Name = "vehicleIdDataGridViewTextBoxColumn";
            // 
            // TeslaAutomobiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 415);
            this.Controls.Add(this.dataView);
            this.Controls.Add(this.OperationButton);
            this.Controls.Add(this.VehicleTypeText);
            this.Controls.Add(this.VehicleNameText);
            this.Controls.Add(this.VehicleDescriptionText);
            this.Controls.Add(this.VehicleType);
            this.Controls.Add(this.VehicleName);
            this.Controls.Add(this.VehicleDescription);
            this.Controls.Add(this.AddOption);
            this.Controls.Add(this.UpdateOption);
            this.Controls.Add(this.DeleteOption);
            this.Controls.Add(this.SearchOption);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TeslaAutomobiles";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.TeslaAutomobiles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teslaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleDetailBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton SearchOption;
        private System.Windows.Forms.RadioButton DeleteOption;
        private System.Windows.Forms.RadioButton UpdateOption;
        private System.Windows.Forms.RadioButton AddOption;
        private System.Windows.Forms.Label VehicleDescription;
        private System.Windows.Forms.Label VehicleName;
        private System.Windows.Forms.Label VehicleType;
        private System.Windows.Forms.TextBox VehicleDescriptionText;
        private System.Windows.Forms.TextBox VehicleNameText;
        private System.Windows.Forms.TextBox VehicleTypeText;
        private System.Windows.Forms.Button OperationButton;
        private System.Windows.Forms.DataGridView dataView;
        private TeslaDataSet teslaDataSet;
        private System.Windows.Forms.BindingSource vehicleDetailBindingSource;
        private TeslaDataSetTableAdapters.VehicleDetailTableAdapter vehicleDetailTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleIdDataGridViewTextBoxColumn;
    }
}

