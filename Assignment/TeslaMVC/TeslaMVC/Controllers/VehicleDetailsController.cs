﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeslaMVC.Models;

namespace TeslaMVC.Controllers
{
    public class VehicleDetailsController : Controller
    {
        private TeslaEntities teslaEntities = new TeslaEntities();

        public string Error()
        {
            return "Data already available";
        }
        // GET: VehicleDetails
        public ActionResult Index()
        {
            try
            {
                var vehicleDetails = teslaEntities.VehicleDetails.Include(v => v.Vehicle);
                return View(vehicleDetails.ToList());
            }
            catch
            {
                return View("Error");
            }
        }
        // GET: VehicleDetails/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return View("Not Found");
                }
                VehicleDetail vehicleDetail = teslaEntities.VehicleDetails.Find(id);
                if (vehicleDetail == null)
                {
                    return View("Not found");
                }
                return View(vehicleDetail);
            }
            catch
            {
                return View("Not Found");
            }
        }

        // GET: VehicleDetails/Create
        public ActionResult Create()
        {
            try
            {
                
                ViewBag.VehicleId = new SelectList(teslaEntities.Vehicles, "VehicleID", "VehicleType");
                return View();
            }
            catch
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,VehicleName,VehicleDescription,VehicleId")] VehicleDetail vehicleDetail)
        {
            try
            {
                TeslaEntities teslaEntities = new TeslaEntities();
                string name = vehicleDetail.VehicleName;
                var vehicles = (from vehicle in teslaEntities.VehicleDetails where vehicle.VehicleName == name select vehicle).ToList();
                    if (ModelState.IsValid)
                    {
                        if (vehicles.Count > 0) 
                        {
                            ViewBag.Duplicate = "duplicate";
                            return RedirectToAction("Error");
                        }
                        else
                        {
                            teslaEntities.VehicleDetails.Add(vehicleDetail);
                            teslaEntities.SaveChanges();
                            return RedirectToAction("Index");
                            
                        }
                    }

                    ViewBag.VehicleId = new SelectList(teslaEntities.Vehicles, "VehicleID", "VehicleType", vehicleDetail.VehicleId);
                    return View(vehicleDetail);
               
               
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: VehicleDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                VehicleDetail vehicleDetail = teslaEntities.VehicleDetails.Find(id);
                if (vehicleDetail == null)
                {
                    return HttpNotFound();
                }
                ViewBag.VehicleId = new SelectList(teslaEntities.Vehicles, "VehicleID", "VehicleType", vehicleDetail.VehicleId);
                return View(vehicleDetail);
            }
            catch
            {
                return View("Error");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,VehicleName,VehicleDescription,VehicleId")] VehicleDetail vehicleDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    teslaEntities.Entry(vehicleDetail).State = EntityState.Modified;
                    teslaEntities.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.VehicleId = new SelectList(teslaEntities.Vehicles, "VehicleID", "VehicleType", vehicleDetail.VehicleId);
                return View(vehicleDetail);
            }
            catch
            {
                return View("error");
            }
        }

        // GET: VehicleDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                VehicleDetail vehicleDetail = teslaEntities.VehicleDetails.Find(id);
                if (vehicleDetail == null)
                {
                    return HttpNotFound();
                }
                return View(vehicleDetail);
            }
            catch
            {
                return View("error");
            }
        }

        // POST: VehicleDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                VehicleDetail vehicleDetail = teslaEntities.VehicleDetails.Find(id);
                teslaEntities.VehicleDetails.Remove(vehicleDetail);
                teslaEntities.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View("error");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                teslaEntities.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
