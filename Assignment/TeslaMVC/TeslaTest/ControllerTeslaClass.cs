﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TeslaMVC.Controllers;
using System.Web.Mvc;
using TeslaMVC.Models;

namespace TeslaTest
{
    [TestFixture]
    public class ControllerTeslaClass
    {
        [Test]
        public void TestVehicleIndex()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Index() as ViewResult ;
            Assert.IsNotNull(viewResult);
        }
        [Test]
        public void TestVehicleCreate()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Create() as ViewResult;
            Assert.IsNotNull(viewResult);
        }
        [Test]
        public void TestVehicleCreateSecond()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Create(null) as ViewResult;
            Assert.IsNotNull(viewResult);
        }
        [Test]
        public void TestVehicleDetails()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Details(null) as ViewResult;
            Assert.IsNotNull(viewResult);
        }
        [Test]
        public void TestVehicleDelete()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Delete(1) as ViewResult;
            Assert.IsNotNull(viewResult);
        }
        [Test]
        public void TestVehicleError()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            string message = vehicleDetailsController.Error() as string;
            Assert.IsNotNull(message);
        }
        [Test]
        public void TestVehicleEdit()
        {
            VehicleDetailsController vehicleDetailsController = new VehicleDetailsController();
            ViewResult viewResult = vehicleDetailsController.Edit(1) as ViewResult;
            Assert.IsNotNull(viewResult);
        }
       
       
    }
}
